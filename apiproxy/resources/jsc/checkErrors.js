var response = context.getVariable("response.content");

if (typeof response != 'undefined') {
    var body = JSON.parse(response);


    var errCode = body.errors[0].error_code;
    var errorDescription = body.errors[0].description;

    print(errCode);
    print(errorDescription);

    var retriableErrorCodes = {
        "1002-002": "R",
        "1002-003": "R",
        "1002-004": "R",
        "1002-005": "R",
        "1002-006": "R",
        "1002-007": "R",
        "1002-008": "R",
        "1002-009": "R",
        "1002-010": "R",
        "1002-101": "R",
        "1002-102": "R",
        "1002-103": "R",
        "1002-104": "R",
        "1002-106": "R",
        "1002-108": "R",
        "1002-109": "R",
        "1002-110": "R",
        "1002-111": "R",
        "1002-112": "R",
        "1002-113": "R",
        "1002-114": "R",
        "1002-115": "R",
        "1002-117": "R",
        "1002-118": "R",
        "408-001": "R",
        "409-001": "R",
        "500-001": "R",
        "500-002": "R",
        "503-001": "R",
        "504-001": "R",
        "401-002": "E",
    };

    var informIICS = 'N';

    if (errCode == "1002-001" || errCode == "1002-002" || errCode == "1002-100") {
        context.setVariable("response.status.code", 200);
        context.setVariable("status", 'N');
    } else if (errCode == "400-016") {
        context.setVariable("response.status.code", 200);
        context.setVariable("status", 'U');
    } else if (retriableErrorCodes[errCode] == 'R' || retriableErrorCodes[errCode] == 'E' || retriableErrorCodes[errCode] == 'RE') {
        if (retriableErrorCodes[errCode] == 'R') {
            context.setVariable("response.status.code", 500);
        }
        informIICS = ('informIICS = ' + retriableErrorCodes[errCode]);
        context.setVariable("errorDetail", errorDescription + ' ' + informIICS);
    } else {
        context.setVariable("errorDetail", errorDescription + ' ' + informIICS);
    }
}