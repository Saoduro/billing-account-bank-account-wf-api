var response = context.getVariable("response.content");

if (typeof response != 'undefined') {
var body = JSON.parse(response);

context.setVariable("transactionDTS", body.transaction_datetime.toString().replace('Z', '.000'));
context.setVariable("validationStatus", body.calculated_response);
context.setVariable("routingNumber", body.bank_routing_number);
context.setVariable("accountNumber", body.bank_account_number);
}